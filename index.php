<?php

	require "./config.php";
	
	$url=(isset($_GET["url"]) && $_GET["url"] != null) ? $_GET["url"] : "Index/index";
	//print_r($url);
	$url=explode("/", $url);
	
	
	$controller=(isset($url[0])) ? $url[0]."_controller" : "Index_controller";
	$method=(isset($url[1]) && $url[1] != null) ? $url[1] : "index";
	$param=(isset($url[2]) && $url[2] != null) ? $url[2] : null;

	spl_autoload_register(function($class){
		if(file_exists(LIBS.$class.".php")){
			require LIBS.$class.".php";
		}elseif (MODELS.$class.".php") {
			require MODELS.$class.".php";
		}else{
			if(file_exists(BS.$class.".php")){
				require BS.$class.".php";
			}else{
				exit("la clase ".$class." no se ha definido");
			}
		}
	});


	/*echo "Cont: ".$controller;
	echo "<br>meth: ".$method;
	echo"<br>param: ".$param;*/

	$path="./controllers/".$controller.".php";

	if(file_exists($path)){
		require $path;
		$controller = new $controller();

		if(method_exists($controller, $method)){
			if($param != null){
				$controller->{$method}($param);
			}else{
				$controller->{$method}();
			}

		}else{

			exit("Invalid Method");
		}



	}else{
		exit("Invalid Controller");
	}



?>